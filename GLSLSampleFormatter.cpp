// GLSLSampleFormatter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
	const char* iFilePath = "poisson_sample_256.txt";
	const char* oFilePath = "poisson_sample_256_glsl.text";

	string outputFile = "";
	fstream iPoissonSampleFile(iFilePath);
	if (iPoissonSampleFile.is_open())
	{
		// read data
		string line;
		int numLine = 0;
		while (getline(iPoissonSampleFile, line)) {
			line.erase(line.begin() + line.size() -2, line.end());
			string newLine = "vec2(";
			newLine.append(line);
			newLine.append(")");
			if (!iPoissonSampleFile.eof()) newLine.append(", ");
			cout << newLine << endl;

			outputFile.append(newLine);
			if ((numLine + 1) % 4 == 0) outputFile.append("\n");

			numLine++;
		}
		iPoissonSampleFile.close();

		// write data
		// write data
		ofstream oPoissonSampleFile(oFilePath);
		if (oPoissonSampleFile.is_open())
		{
			oPoissonSampleFile << outputFile;
			oPoissonSampleFile.close();
		}
		else
		{
			cout << "Unable to open " << oFilePath << endl;
		}
	}
	else {
		cout << "Unable to open " << iFilePath << endl;
	}

	while (1) {}

    return 0;
}

